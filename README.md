Parte 1:
Design Overview o Complete Specification: 
Estructuras del sistema:
Estructura Nodo es la que se usa para manejar la cola que es usada para obtener el numero de page faults en el algoritmo fifo, maneja operaciones de insertar, quitar,saber si esta vacia, imprimir y buscar.
Estructura configuracion en esta se almacenan los datos de la lectura del archivo, se hace un parse del archivo en esta estructura para manejar como una variable global en todo el sistema.
Estructura resultado en esta se almacenan todos los calculos necesarios para poder presentarlos en el apartado final.

El codigo se compone de distintas partes generales:
Lectura del archivo: en esta parte se toma linea por linea el archivo y valido si posee un igual luego se hace un split por el igual si esta esta contenida en una lista de los parametros que debe el archivo, si es asi en una lista vacia de ceros coloco un uno en esa posicion, para validar que esta correcto el numero de parametros sumo esa lista y si es igual a 8 entonces los parametros estan correctos.

Calculos:
Offset bits-> Es el valor del PAGESIZE parseado de la estructura configuracion.
Page bits-> Es el valor del LAS parseado de la estructura configuracion menos el offset.
Frame bits-> Es el valor del PAS parseado de la estructura configuracion menos el offset.
Max number of Page Table Entries-> 2^Page bits
Max number of frames-> 2^Frame bits
Size in bytes required to represent the Page Table-> (2^Page bits*Frame bits)/8
Number of pages marked as valid for the process-> Se divide el proceso para/PAGESIZE
Reference string based on the address sequence-> se toma cada valor de la lista PROCESS_MEMREF/PAGESIZE

Algoritmos:
FIFO-> Para este algoritmo se hace uso de una cola para el manejo del algoritmo por cada elemento nuevo se lo ingresa si no se encuentra en la cola se valida si esta vacia y se lo inserta caso contrario se extrae un elemento se lo inserta y se anota el fallo de pagina, caso contrario se registra el hit.
LRU->Para este algoritmo se usan dos listas una donde se almacenan los elementos la otra inicializada en -1 sirve para controlar si en la iteracion hacia atras ya se encontro un elemento si esto pasa se cambia a 1, ademas se hace uso de un contador para controlar cuando este es igual al numero de frames entonces se lo debe sacar de la lista.
OPTIMAL->Para este algoritmo se usan dos listas una donde se almacenan los elementos la otra inicializada en -1 sirve para controlar si en la iteracion hacia atras ya se encontro un elemento si esto pasa se cambia setea por el valor de un contador, luego se itera esta cola y en la posicion que contenga un -1 o el valor igual al numero de frame es reemplazada

Known bugs or problems: 
En la lectura del archivo puede contener errores puesto que se realizaron validacion de casos comunes pero si se puede hacer un analisis mas extenso del codigo es posible que se puedan hacer caer el sistema.

Parte 2:
Design Overview o Complete Specification: 
Estructuras del sistema:
Estructura Nodo es la que se usa para manejar la lista enlazada, maneja operaciones de insertar, quitar, actualizar, imprimir y buscar.
Estructura Datos_archivo en esta estructura se almacenan los valores leidos del archivo ademas contiene variables que sirven para mostrarla presentacion final.
Estructura Operaciones en esta estructura se almacenan el valor, posicion leidos del archivo asi como su tipo_operacion 1: Insertar ;2: Actualizar ;3: Eliminar;4: Buscar y el tiempo de espera que es un valor random entre 1 y 3 esto sirve para simular el tiempo que le toma a cada operacion terminar.

Se manejaron los siguientes mutex: mutex_insert,mutex_actualizar,mutex_eliminar, son usadas para asegurar que el sistema se ejecute de manera correcta

El codigo se compone de distintas partes generales:
Lectura del archivo: en esta parte se toma linea por linea el archivo y valido si posee un igual luego se hace un split por el igual si esta esta contenida en una lista de los parametros que debe el archivo, si es asi en la estructura Datos_archivo hay un puntero que contiene una lista de punteros de la estructura Operaciones donde conocemos cuantas operaciones se ejecutaran.

Asignacion de operaciones a los hilos:
Con la lista de operaciones a ejecutarse se validan las distintas operaciones y se asignan sus operaciones pertinentes ya sean insertar, buscar, etc.

Control de las distintas condiciones de carrera:
Cada vez que se inserta un elemento se adquiere el candado y se libera una vez completada la operacion.
Cada vez que se actualiza un elemento se adquiere el candado y se libera una vez completada la operacion.
Cada vez que se busca se adquiere el candado de actualizar y se libera una vez completada la operacion.
Cada vez que se elimina un elemento se adquiere el un candado de insertar, actualizar y eliminar y se liberan una vez completada la operacion.

Known bugs or problems: 
En la lectura del archivo puede contener errores puesto que se realizaron validacion de casos comunes pero si se puede hacer un analisis mas extenso del codigo es posible que se puedan hacer caer el sistema.
Cuando se elimina un elemento, al tomar los candados bloquea varias operaciones que se pueden ejecutar en paralelo, aunque sea parte de los requerimientos alenta el sistema en general.

