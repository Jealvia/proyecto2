/**
  @file main.c
  @brief Archivo main de la primera parte del proyecto
  @author Alvia Apráez Julio
  @date 22/01/2020, 02/02/2020

*/
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include<ctype.h>
//gcc parte1.c -o parte1 -lm

/**
 * Constantes del sistema
 */
#define VALORPALABRA 1000

/**
 * Estructura para la implementacion de una cola para FIFO.
 */
typedef struct Nodo {
    int datos;
    struct Nodo *siguiente;
} nodo_t;

/**
 * Estructura para manejar la configuracion.
 */
typedef struct configuracion {
  double LAS;
  double PAS;
  double PAGESIZE;
  char * PROCESS_NAME;
  double PROCESS_SIZE;
  int * PROCESS_MEMREF;
  int  ALGORITHM;//1: FIFO, 2: LRU, 3: OPTIMAL
  double PAGE_FRAMES;
  int NUMERO_PROCESOS;
}
configuracion_t;

/**
 * Estructura para manejar los resultados
 */
typedef struct resultado {
  double LA;//Logical address structure
  double PA;//Physical address structure
  double offset;//offset
  int PTE;//Max number of Page Table Entries
  int MNF;//Max number of frames
  int PTS;//Size in bytes required to represent the Page Table
  double SAS;//Reference string based on the address sequence
  double NHit;//Summarise number of hits and page fault
  int PAGINAS_VALIDAS;
  int PAGINAS_INVALIDAS;
  int * ADDRESS_SEQUENCE;
  int PAGE_FAULTS;
  int HITS;
}
resultado_t;

/**
 * Variable globales del sistema.
 */
configuracion_t configuracion;
resultado_t resultados_parciales;
char * nombre_archivo;
char cadena[VALORPALABRA];
nodo_t *raiz=NULL;
nodo_t *fondo=NULL;

/**
  @brief Funcion que valida si la cola se encuentra vacia

  @param No recibe parametros

  @returns un entero 0 fallo 1 exitoso.
*/
int vacia(){
  if (raiz == NULL)
      return 1;
  else
      return 0;
}

/**
  @brief Funcion que permite insertar elementos a la cola

  @param int data recibe el valor a ingresar

  @returns No retorna nada
*/
void insertar(int dato){
    nodo_t *nuevo;
    nuevo=malloc(sizeof(nodo_t));
    nuevo->datos=dato;
    nuevo->siguiente=NULL;
    if (vacia()){
      raiz = nuevo;
      fondo = nuevo;
    }
    else{
      fondo->siguiente = nuevo;
      fondo = nuevo;
    }
}

/**
  @brief Funcion que permite buscar elementos en la cola

  @param int data recibe el valor a buscar

  @returns Retorna un entero 0 fallo 1 exitoso.
*/
int buscar(int dato){
  int resultado=0;
  nodo_t *reco = raiz;
  while (reco != NULL){
    if(dato==reco->datos){
        resultado=1;
    }
    reco = reco->siguiente;
  }
  return resultado;
}

/**
  @brief Funcion que permite sacar elementos en la cola

  @param No recibe parametros

  @returns Retorna un entero -1 fallo el valor extraidoen caso exitoso.
*/
int extraer(){
  if (!vacia()){
    int informacion = raiz->datos;
    nodo_t *bor = raiz;
    if (raiz == fondo){
      raiz = NULL;
      fondo = NULL;
    }else{
      raiz = raiz->siguiente;
    }
    free(bor);
    return informacion;
  }else{
    return -1;
  }
}

/**
  @brief Funcion que imprime la cola

  @param No recibe parametros

  @returns No retorna nada
*/
void imprimir(){
  nodo_t *reco = raiz;
  printf("|");
  while (reco != NULL){
    printf("  %i  |", reco->datos);
    reco = reco->siguiente;
  }
  printf("\n");
}

/*
  Lista de contenido del archivo
*/
char * lista_contenidos[] = {
  "LAS",
  "PAS",
  "PROCESS_NAME",
  "PROCESS_SIZE",
  "PROCESS_MEMREF",
  "ALGORITHM",
  "PAGE_FRAMES"
};

/**
  @brief Convierte el valor de LAS, PAS, PAGESIZE,PROCESS_SIZE en bits
  para los calculos necesarios de la estructura de direcciones.

  @param char recibe una cadena donde se encuentra el tamano de los
  distintos valores de LAS, PAS, PAGESIZE,PROCESS_SIZE.

  @returns double valor en bits de la data.
*/
double parsear_pesos(char data[]){
  int cadena_len=strlen(data);
  char peso[VALORPALABRA]="";
  int peso_counter=0;
  double resultado_mem=0;
  for (size_t i = 0; i < cadena_len; i++)
  {
    if(isdigit(data[i])!=0){
      peso[peso_counter]=data[i];
      peso_counter++;
    }else if(data[i]=='G'){
      resultado_mem+=30;
    }else if(data[i]=='K'){
      resultado_mem+=10;
    }else if(data[i]=='M'){
      resultado_mem+=20;
    }else if(data[i]=='B'){
      resultado_mem=resultado_mem*1;
    }
  }
  double tmp_peso=(double)atoi(peso);
  double res_peso=log10(tmp_peso)/log10(2);
  
  resultado_mem+=res_peso;
  return resultado_mem;
}

/**
  @brief Convierte el valor de LAS, PAS, PAGESIZE,PROCESS_SIZE en bits
  para los calculos necesarios de la estructura de direcciones.

  @param char recibe una cadena donde se encuentra el tamano de los
  distintos valores de LAS, PAS, PAGESIZE,PROCESS_SIZE.

  @returns double valor en bits de la data.
*/
double parsear_process_size(char data[]){
  int cadena_len=strlen(data);
  char peso[VALORPALABRA]="";
  int peso_counter=0;
  double resultado_mem=0;
  for (size_t i = 0; i < cadena_len; i++)
  {
    if(isdigit(data[i])!=0){
      peso[peso_counter]=data[i];
      peso_counter++;
    }else if(data[i]=='G'){
      resultado_mem+=30;
    }else if(data[i]=='K'){
      resultado_mem+=10;
    }else if(data[i]=='M'){
      resultado_mem+=20;
    }else if(data[i]=='B'){
      resultado_mem=resultado_mem*1;
    }
  }
  double tmp_peso=(double)atoi(peso);
  resultado_mem+=tmp_peso;
  return resultado_mem;
}

/**
  @brief Convierte el valor de PROCESS_MEMREF que llega como cadena
  y lo asigna a la estructura de en forma de lista para su posterior uso.

  @param * char recibe una cadena donde se encuentra PROCESS_MEMREF 

  @returns 
*/
void parsear_memref(char data[]){
  int resultado[100];

  char * token = strtok(data, ",");
  
  int valor=atoi(token);
  //resultado[contador]=valor;
  //printf("%d--",valor);
  //configuracion.PROCESS_MEMREF[contador]=valor;
  int contador=0;
  while( token != NULL ) {
    int valor=atoi(token);
    resultado[contador]=valor;
    configuracion.PROCESS_MEMREF[contador]=valor;
    token = strtok(NULL, ",");
    contador++;
  }
  //printf("\n");
  configuracion.NUMERO_PROCESOS=contador;
  //configuracion.PROCESS_MEMREF=resultado;
}

/**
  @brief Calcula el numero de paginas invalidas del algoritmo de reemplazo de pagina

  @param double proceso tamano del proceso, double bytes_por_pagina valor del PAGESIZE

  @returns numero de paginas invalidas
*/
int calcular_paginas_invalidas(double proceso,double bytes_por_pagina){
  int resultado=0;
  double calculo=proceso/pow (2, (int)bytes_por_pagina);
  double potencia_calculo=log10(calculo)/log10(2);
  int calculo_potencia=pow (2, (int)potencia_calculo);
  if(calculo_potencia>calculo){
    if(calculo>(int)calculo){
      resultado=calculo_potencia-((int)calculo+1);
    }else{
      resultado=calculo_potencia-((int)calculo);
    }
  }else{
    if(calculo>(int)calculo){
      resultado=pow (2, (int)potencia_calculo+1)-((int)calculo+1);
    }else{
      resultado=pow (2, (int)potencia_calculo+1)-((int)calculo);
    }
  }
  return resultado;
}

/**
  @brief Calcula el numero de paginas invalidas del algoritmo de reemplazo de pagina

  @param double proceso tamano del proceso, double bytes_por_pagina valor del PAGESIZE

  @returns numero de paginas validas
*/
int calcular_paginas_validas(double proceso,double bytes_por_pagina){
  int resultado=0;
  double calculo=proceso/pow (2, (int)bytes_por_pagina);
  double potencia_calculo=log10(calculo)/log10(2);
  int calculo_potencia=pow (2, (int)potencia_calculo);
  if(calculo_potencia>calculo){
    if(calculo>(int)calculo){
      int invalidas=calculo_potencia-((int)calculo+1);
      resultado=calculo_potencia-invalidas;
    }else{
      int invalidas=calculo_potencia-((int)calculo);
      resultado=calculo_potencia-invalidas;
    }
  }else{
    if(calculo>(int)calculo){
      int invalidas=pow (2, (int)potencia_calculo+1)-((int)calculo+1);
      resultado=pow (2, (int)potencia_calculo+1)-invalidas;
    }else{
      int invalidas=pow (2, (int)potencia_calculo+1)-((int)calculo);
      resultado=pow (2, (int)potencia_calculo+1)-invalidas;
    }
  }
  return resultado;
}

/**
  @brief Se realizan los calculos previos a la presentacion y se los asigna
    en la estructura de resultados

  @return No retorna nada
*/
void calculos_generales(){
  resultados_parciales.offset=configuracion.PAGESIZE;
  resultados_parciales.LA=configuracion.LAS-resultados_parciales.offset;
  resultados_parciales.PA=configuracion.PAS-resultados_parciales.offset;
  resultados_parciales.PTE=pow (2, (int)resultados_parciales.LA);
  resultados_parciales.MNF=pow (2, (int)resultados_parciales.PA);
  resultados_parciales.PTS=(pow (2, (int)resultados_parciales.LA)*(int)resultados_parciales.PA)/8;
  resultados_parciales.PAGINAS_VALIDAS=calcular_paginas_validas(configuracion.PROCESS_SIZE,configuracion.PAGESIZE);
  resultados_parciales.PAGINAS_INVALIDAS=calcular_paginas_invalidas(configuracion.PROCESS_SIZE,configuracion.PAGESIZE);
  
  double divisor=pow (2, configuracion.PAGESIZE);
  for (size_t i = 0; i < configuracion.NUMERO_PROCESOS; i++)
  {
    int calculo=configuracion.PROCESS_MEMREF[i]/divisor;
    resultados_parciales.ADDRESS_SEQUENCE[i]=calculo;
  }
}

/**
  @brief Inicializa una estructura calc_lineas_t para el uso de del contador de lineas.
        Se encarga de asignar los parametros iniciales a la estructura de calc_lineas_t que
        se encarga de la lectura del archivo, en caso de error cierra el programa por el 
        envio erroneo de la ruta.

  @returns calc_lineas_t estructura inicializada.
*/
int inicializar_estructura_configuracion() {
  configuracion.PROCESS_MEMREF=(int*) malloc(100*sizeof(int));
  resultados_parciales.ADDRESS_SEQUENCE=(int*) malloc(100*sizeof(int));
  FILE * fich;
  fich = fopen(nombre_archivo, "r");
  char * lista_contenidos[] = {
    "LAS",
    "PAS",
    "PAGESIZE",
    "PROCESS_NAME",
    "PROCESS_SIZE",
    "PROCESS_MEMREF",
    "ALGORITHM",
    "PAGE_FRAMES"
  };
  char * lista_resultados[40];
  int lista_encontrados[] = {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
  };
  //Lee línea a línea y escribe en pantalla hasta el fin de fichero
  while (fgets(cadena, 1024, (FILE * ) fich)) {
    
    if(strstr(cadena, "=") != NULL){
      int tam=strlen(cadena);
      int bandera=1;
      char variable[VALORPALABRA]="";
      char data[VALORPALABRA]="";
      int var=0;
      int dat=0;
      for (int i = 0; i < tam; i++){
        if(bandera==2){
          if(cadena[i]=='\n'){
            bandera=4;
          }else{
            data[dat]=cadena[i];
            dat++;
          }
        }
        if(bandera==1){
          if(cadena[i]=='='){
            bandera=2;
          }else{
            variable[var]=cadena[i];
            var++;
          }
        }
      }
      for (size_t i = 0; i < 8; i++){
        if (strcmp(lista_contenidos[i], variable) == 0) {
          switch (i){
          case 0:
            configuracion.LAS=parsear_pesos(data);
            if(lista_encontrados[i] == 0){
              lista_encontrados[i] = 1;
            }
            break;
          case 1:
            configuracion.PAS=parsear_pesos(data);
            if(lista_encontrados[i] == 0){
              lista_encontrados[i] = 1;
            }
            break;
          case 2:
            configuracion.PAGESIZE=parsear_pesos(data);
            if(lista_encontrados[i] == 0){
              lista_encontrados[i] = 1;
            }
            break;
          case 3:
            configuracion.PROCESS_NAME=data;
            if(lista_encontrados[i] == 0){
              lista_encontrados[i] = 1;
            }
            break;
          case 4:
            configuracion.PROCESS_SIZE=parsear_process_size(data);
            if(lista_encontrados[i] == 0){
              lista_encontrados[i] = 1;
            }
            break;
          case 5: ;
            parsear_memref(data);
            if(lista_encontrados[i] == 0){
              lista_encontrados[i] = 1;
            }
            break;
          case 6:
            if(strstr(data,"FIFO")!= NULL){
              configuracion.ALGORITHM=1;
              if(lista_encontrados[i] == 0){
                lista_encontrados[i] = 1;
              }
            }else if(strstr(data,"LRU")!= NULL){
              configuracion.ALGORITHM=2;
              if(lista_encontrados[i] == 0){
                lista_encontrados[i] = 1;
              }
            }else if(strstr(data,"OPTIMAL")!= NULL){
              configuracion.ALGORITHM=3;
              if(lista_encontrados[i] == 0){
                lista_encontrados[i] = 1;
              }
            }
            
            break;
          case 7:
            configuracion.PAGE_FRAMES=(double)atoi(data);
            if(lista_encontrados[i] == 0){
              lista_encontrados[i] = 1;
            }
            break;
          }
          lista_resultados[i]=data;
          
        }
      }
      
    }
  }

  int sumador=0;
  for (int i = 0; i < 8; i++) {
    sumador+=lista_encontrados[i];
  }
  if(sumador==8){
    return 1;
  }else
  {
    printf("Archivo de configuracion erróneo\n");
    printf("Faltaron los siguientes elementos en el archivo:\n");
    for (int i = 0; i < 8; i++) {
      if(lista_encontrados[i]==0){
        printf("Elemento: %s\n",lista_contenidos[i]);
      }
      
    }
    return -1;
  }

}

/**
  @brief Muestra por pantalla el manejo del algoritmo fifo, se hace uso de una cola para el manejo
        del algoritmo por cada elemento nuevo se lo ingresa si no se encuentra en la cola se valida si 
        esta vacia y se lo inserta caso contrario se extrae un elemento se lo inserta y se anota el fallo
        de pagina, caso contrario se registra el hit

  @returns No retorna nada
*/
void page_replacement_fifo(){
  int frames=(int)configuracion.PAGE_FRAMES;
  int *cola=(int*) malloc(frames*sizeof(int));
  int bandera=0;
  int page_fault=0;
  int actuales=0;
  int hits=0;
  for (size_t j = 0; j < frames; j++){
    cola[j]=-1;
  }
  for (int i = 0; i < configuracion.NUMERO_PROCESOS; i++){
    printf("Process %d -->",resultados_parciales.ADDRESS_SEQUENCE[i]);
    bandera=buscar(resultados_parciales.ADDRESS_SEQUENCE[i]);
    if(bandera==0){
      page_fault++;
      insertar(resultados_parciales.ADDRESS_SEQUENCE[i]);
      if(actuales==frames){
        extraer();
      }else{
        actuales++;
      }
    }else{
      hits++;
    }
    bandera=0;
    imprimir();
  }
  resultados_parciales.HITS=hits;
  resultados_parciales.PAGE_FAULTS=page_fault;
}

/**
  @brief Muestra por pantalla el manejo del algoritmo lru, se usan dos listas una donde se almacenan los elementos
  la otra inicializada en -1 sirve para controlar si en la iteracion hacia atras ya se encontro un elemento si esto pasa
  se cambia a 1, ademas se hace uso de un contador para controlar cuando este es igual al numero de frames entonces se lo
  debe sacar de la lista.

  @returns No retorna nada
*/
void page_replacement_lru(){
  int frames=(int)configuracion.PAGE_FRAMES;
  int *cola=(int*) malloc(frames*sizeof(int));
  int *cola_validacion=(int*) malloc(frames*sizeof(int));
  int bandera=0;
  int page_fault=0;
  int hits=0;
  int contador=0;
  int elementos=0;
  for (size_t j = 0; j < frames; j++){
    cola[j]=-1;
    cola_validacion[j]=-1;
  }
  for (int i = 0; i < configuracion.NUMERO_PROCESOS; i++){
    printf("Process %d  ->",resultados_parciales.ADDRESS_SEQUENCE[i]);
    for (int j = 0; j < frames; j++){
      if(cola[j]==resultados_parciales.ADDRESS_SEQUENCE[i]){
        bandera=1;
      }
    }
    if(bandera==0){
      page_fault++;
      if(elementos==frames){
        for (int k = i-1; k >= 0; k--){
          for (int l = 0; l < frames; l++){
            if(cola[l]==resultados_parciales.ADDRESS_SEQUENCE[k] && cola_validacion[l]==-1){
              contador++;
              cola_validacion[l]=1;
            }
            if(contador==frames){
              cola[l]=resultados_parciales.ADDRESS_SEQUENCE[i];
              contador=0;
            }
          }
        }
        for (size_t j = 0; j < frames; j++){
          cola_validacion[j]=-1;
        }
        contador=0;
      }else{
        cola[elementos]=resultados_parciales.ADDRESS_SEQUENCE[i];
        elementos++;
      }
    }else{
      hits++;
    }
    bandera=0;
    printf("|");
    for (size_t j = 0; j < frames; j++){
      if(cola[j]!=-1){
        printf("  %d  |",cola[j]);
      }
    }
    printf("\n");
  }
  resultados_parciales.HITS=hits;
  resultados_parciales.PAGE_FAULTS=page_fault;
}

/**
  @brief Muestra por pantalla el manejo del algoritmo optimo, se usan dos listas una donde se almacenan los elementos
  la otra inicializada en -1 sirve para controlar si en la iteracion hacia atras ya se encontro un elemento si esto pasa
  se cambia setea por el valor de un contador, luego se itera esta cola y en la posicion que contenga un -1 o el valor igual
  al numero de frame es reemplazada

  @returns No retorna nada
*/
void page_replacement_optimo(){
  int frames=(int)configuracion.PAGE_FRAMES;
  int *cola=(int*) malloc(frames*sizeof(int));
  int *cola_validacion=(int*) malloc(frames*sizeof(int));
  int bandera=0;
  int page_fault=0;
  int hits=0;
  int contador=0;
  int elementos=0;
  for (size_t j = 0; j < frames; j++){
    cola[j]=-1;
    cola_validacion[j]=-1;
  }
  for (int i = 0; i < configuracion.NUMERO_PROCESOS; i++){
    printf("Process %d  ->",resultados_parciales.ADDRESS_SEQUENCE[i]);
    for (int j = 0; j < frames; j++){
      if(cola[j]==resultados_parciales.ADDRESS_SEQUENCE[i]){
        bandera=1;
      }
    }
    if(bandera==0){
      page_fault++;
      if(elementos==frames){
        for (int k = i+1; k < configuracion.NUMERO_PROCESOS; k++){
          for (int l = 0; l < frames; l++){
            if(cola[l]==resultados_parciales.ADDRESS_SEQUENCE[k] && cola_validacion[l]==-1){
              contador++;
              cola_validacion[l]=contador;
            }
          }
        }
        int remplazo=0;
        for (size_t j = 0; j < frames; j++){
          if(remplazo==0 && cola_validacion[j]==-1){
            cola[j]=resultados_parciales.ADDRESS_SEQUENCE[i];
            remplazo=1;
          }else if(remplazo==0 && cola_validacion[j]==4){
            cola[j]=resultados_parciales.ADDRESS_SEQUENCE[i];
            remplazo=1;
          }
          cola_validacion[j]=-1;
          
        }
        contador=0;
      }else{
        cola[elementos]=resultados_parciales.ADDRESS_SEQUENCE[i];
        elementos++;
      }
    }else{
      hits++;
    }
    printf("|");
    for (size_t j = 0; j < frames; j++){
      if(cola[j]!=-1){
        printf("  %d  |",cola[j]);
      }
    }
    printf("\n");
    bandera=0;
  }
  resultados_parciales.HITS=hits;
  resultados_parciales.PAGE_FAULTS=page_fault;
}

/**
  @brief Muestra por pantalla los resultados generales del sistema

  @returns No retorna nada
*/
void presentacion_resultados(){
  printf("********Resultados*******\n");
  printf("1. Logical address structure:\n");
  printf("||Page-> %d bits  |",(int)resultados_parciales.LA);
  printf("|Offset-> %d bits  |",(int)resultados_parciales.offset);
  printf("|Total-> %d bits||\n",(int)(resultados_parciales.LA+resultados_parciales.offset));
  printf("\n2. Physical address structure:\n");
  printf("||Frame-> %d bits  |",(int)resultados_parciales.PA);
  printf("|Offset-> %d bits  |",(int)resultados_parciales.offset);
  printf("|Total-> %d bits||\n",(int)(resultados_parciales.PA+resultados_parciales.offset));
  printf("\n3. Max number of Page Table Entries (PTE): %d pages\n",(int)resultados_parciales.PTE);
  printf("\n4. Max number of frames: %d frames\n",resultados_parciales.MNF);
  printf("\n5. Size in bytes required to represent the Page Table: %d bytes\n",resultados_parciales.PTS);
  printf("\n6. Number of pages marked as valid for the process: %d pages\n",resultados_parciales.PAGINAS_VALIDAS);
  printf("\n7. Reference string based on the address sequence: ");
  for (size_t i = 0; i < configuracion.NUMERO_PROCESOS; i++){
    printf("%d ",resultados_parciales.ADDRESS_SEQUENCE[i]);
  }
  printf("\n\n8. Show the output of the page-replacement algorithm ");
  if(configuracion.ALGORITHM==1){
    printf("\nFIFO\n");
    page_replacement_fifo();
  }else if(configuracion.ALGORITHM==2){
    printf("\nLRU\n");
    page_replacement_lru();
  }else if(configuracion.ALGORITHM==3){
    printf("\nOPTIMAL\n");
    page_replacement_optimo();
  }
  
  printf("\n9. Summarise number of hits and page fault: \n");
  printf("Fault: %d\nHits %d\n",resultados_parciales.PAGE_FAULTS,resultados_parciales.HITS);
  /* printf("\nFIFO\n");
    page_replacement_fifo();
  printf("\nLRU\n");
    page_replacement_lru();
  printf("\nOPTIMAL\n");
    page_replacement_optimo(); */
}

int main(int argc, char **argv) {
  if(argc!=2) {
    printf("Parametros: ./main direccion_achivo.txt\n");
    exit(1);
  }
  nombre_archivo=argv[1];
  if(inicializar_estructura_configuracion()==1){
    calculos_generales();
  presentacion_resultados();
  }
  
  return 0;
}
