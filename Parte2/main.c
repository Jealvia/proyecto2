/**
  @file main.c
  @brief Archivo main de la segunda parte del proyecto
  @author Alvia Apráez Julio
  @date 22/01/2020, 02/02/2020

*/
#include <pthread.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string.h>
#include <math.h>
#include<ctype.h>

/**
 * Constantes del sistema
 */
#define VALORPALABRA 1000

/**
 * Estructura para el control de los nodos de la lista enlazada
 */
typedef struct Nodo { 
    int datos; //Valor del nodo
    struct Nodo* siguiente; //Puntero al siguiente elemento
} nodo_t; 

/**
 * Estructura para el control de las operaciones leidas del archivo
 */
typedef struct Operaciones { 
    int valor; //Valor de la operacion a realizar
    int posicion; //Posicion donde se aplicara la operacion
    int tipo_operacion;//1: Insertar ;2: Actualizar ;3: Eliminar;4: Buscar
    int tiempo_espera; //Tiempo de espera para realizar la tarea
} operaciones_t; 

/**
 * Estructura para el control del archivo asi como datos de la presentacion
 */
typedef struct Datos_archivo { 
    operaciones_t * operaciones;
    int numero_operaciones;
    int agregadas;
    int eliminadas;
    int eliminadas_exitosas;
    int eliminadas_fallidas;
    int busquedas;
    int busquedas_exitosas;
    int busquedas_fallidas;
    int actualizaciones;
    int actualizaciones_exitosas;
    int actualizaciones_fallidas;
} datos_archivo_t; 

/**
 * Variables globales del sistema
 */
pthread_mutex_t mutex_insert,mutex_actualizar,mutex_eliminar;
char * nombre_archivo;
char cadena[VALORPALABRA];
datos_archivo_t datos_archivo;
nodo_t *referencia_inicio;

/**
  @brief Funcion para mostrar los elementos en la lista actual

  @param * inicio recibe un puntero hacia el nodo inicio

  @returns No retorna nada.
*/
void imprimir_lista(nodo_t * inicio) {
  nodo_t * actual = inicio;
  int contador=1;
  printf("Lista: ");
  while (actual != NULL) {
    printf("[%d:%d]->",contador, actual->datos);
    actual = actual->siguiente;
    contador++;
  }
  printf("\n");
}

/**
  @brief Funcion para agregar un elemento al final de la lista actual

  @param * ptr recibe un puntero a void que contiene la estructura de operaciones.

  @returns un entero 0 fallo 1 exitoso.
*/
int agregar_elemento(void * ptr) {
  pthread_mutex_lock( & mutex_insert);
  operaciones_t * estructura = (operaciones_t * ) ptr;
  printf("++++++ Agregando elemento: %d\n",(*estructura).valor);
  sleep((*estructura).tiempo_espera);
  nodo_t * actual = referencia_inicio;
  if(actual==NULL){
    actual=(nodo_t*)malloc(sizeof(nodo_t));
    referencia_inicio=actual;
    actual->datos = (*estructura).valor;
    actual->siguiente = NULL;
    imprimir_lista(referencia_inicio);
    pthread_mutex_unlock( & mutex_insert);
    return 1;
  }
  while (actual->siguiente != NULL) {
      actual = actual->siguiente;
  }
  actual->siguiente = (nodo_t *) malloc(sizeof(nodo_t));
  actual->siguiente->datos = (*estructura).valor;
  actual->siguiente->siguiente = NULL;
  imprimir_lista(referencia_inicio);
  pthread_mutex_unlock( & mutex_insert);
  return 1;
}

/**
  @brief Funcion para actualizar un elemento en la posicion indicada de la lista actual

  @param * ptr recibe un puntero a void que contiene la estructura de operaciones.

  @returns un entero 0 fallo 1 exitoso.
*/
int actualizar_elemento(void * ptr) {
  pthread_mutex_lock( & mutex_actualizar);
  operaciones_t * estructura = (operaciones_t * ) ptr;
  printf("oooooo Actualizando elemento %d\n",(*estructura).posicion);
  sleep((*estructura).tiempo_espera);
  if(referencia_inicio==NULL){
    printf("------ No hay elementos en la lista                          X\n");
    datos_archivo.actualizaciones_fallidas++;
    pthread_mutex_unlock( & mutex_actualizar);
    return -1;
  }
  if((*estructura).posicion==1){
    referencia_inicio->datos=(*estructura).valor;
    datos_archivo.actualizaciones_exitosas++;
    imprimir_lista(referencia_inicio);
    pthread_mutex_unlock( & mutex_actualizar);
    return 1;
  }
  nodo_t * actual = referencia_inicio;
  for (int i = 0; i < (*estructura).posicion-1; i++) {
    if (actual->siguiente == NULL) {
      datos_archivo.actualizaciones_fallidas++;
      printf("oooooo Actualizacion fallo: no habia elemento en la posicion: %d                          X\n",(*estructura).posicion);
      pthread_mutex_unlock( & mutex_actualizar);
      return -1;
    }
    actual = actual->siguiente;
  }
  actual->datos=(*estructura).valor;
  datos_archivo.actualizaciones_exitosas++;
  imprimir_lista(referencia_inicio);
  pthread_mutex_unlock( & mutex_actualizar);
  return 1;
}

/**
  @brief Funcion para buscar un elemento dado en la lista actual

  @param * ptr recibe un puntero a void que contiene la estructura de operaciones.

  @returns un entero 0 fallo 1 exitoso.
*/
int buscar_elemento(void * ptr) {
  pthread_mutex_lock( & mutex_actualizar);
  operaciones_t * estructura = (operaciones_t * ) ptr;
  printf("****** Buscando elemento %d \n",(*estructura).valor);
  sleep((*estructura).tiempo_espera);
  nodo_t * actual = referencia_inicio;
  int contador=1;
  while (actual != NULL) {
    //printf("%d--%d ##",actual->datos,(*estructura).valor);
    if(actual->datos==(*estructura).valor){
      printf("****** Elemento %d encontrado en la posicion %d \n",(*estructura).valor,contador);
      datos_archivo.busquedas_exitosas++;
      pthread_mutex_unlock( & mutex_actualizar);
      return 1;
    }
    actual = actual->siguiente;
    contador++;
  }
  datos_archivo.busquedas_fallidas++;
  printf("****** Elemento %d no existe en la lista                          X\n",(*estructura).valor);
  pthread_mutex_unlock( & mutex_actualizar);
  return 1;
}

/**
  @brief Funcion para eliminar un elemento en la posicion indicada de la lista actual

  @param * ptr recibe un puntero a void que contiene la estructura de operaciones.

  @returns un entero 0 fallo 1 exitoso.
*/
int eliminar_elemento(void * ptr) {
  pthread_mutex_lock( & mutex_actualizar);
  pthread_mutex_lock( & mutex_eliminar);
  pthread_mutex_lock( & mutex_insert);
  operaciones_t * estructura = (operaciones_t * ) ptr;
  printf("------ Eliminando elemento en la posicion %d \n",(*estructura).posicion);
  sleep((*estructura).tiempo_espera);
  int resultado = -1;
  nodo_t * actual = referencia_inicio;
  nodo_t * tmp_nodo = NULL;
  if ((*estructura).posicion == 1) {
    int resultado = -1;
    nodo_t * actual = NULL;
    if (referencia_inicio == NULL) {//No hay ningun elemento en la lista
      datos_archivo.eliminadas_fallidas++;
      printf("------ No hay elementos en la lista                          X\n");
      pthread_mutex_unlock( & mutex_insert);
      pthread_mutex_unlock( & mutex_eliminar);
      pthread_mutex_unlock( & mutex_actualizar);
      return -1;
    }
    actual = referencia_inicio->siguiente;
    resultado =0;
    referencia_inicio=actual;
    datos_archivo.eliminadas_exitosas++;
    imprimir_lista(referencia_inicio);

    pthread_mutex_unlock( & mutex_insert);
    pthread_mutex_unlock( & mutex_eliminar);
    pthread_mutex_unlock( & mutex_actualizar);
    return resultado;
  }
  for (int i = 0; i < (*estructura).posicion-1; i++) {
    if (actual->siguiente == NULL) {
      datos_archivo.eliminadas_fallidas++;
      printf("------ En la posicion %d no hay elemento                          X\n",(*estructura).posicion);
      pthread_mutex_unlock( & mutex_insert);
      pthread_mutex_unlock( & mutex_eliminar);
      pthread_mutex_unlock( & mutex_actualizar);
      return resultado;
    }
    tmp_nodo=actual;
    actual = actual->siguiente;
  }
  tmp_nodo->siguiente = actual->siguiente;
  free(actual);
  resultado=0;
  datos_archivo.eliminadas_exitosas++;
  imprimir_lista(referencia_inicio);
  pthread_mutex_unlock( & mutex_insert);
  pthread_mutex_unlock( & mutex_eliminar);
  pthread_mutex_unlock( & mutex_actualizar);
  return resultado;
}

/**
  @brief Funcion para la lectura del archivo, toma el nombre del archivo y lo itera en busca de coincidencias
  para agregar operaciones a la estructura datos_archivo_t asi como el total de operaciones leidas, en caso de
  que una cadena no concuerde continua con la siguiente linea.

  @param No recibe parametros

  @returns un entero 0 fallo cuando la ruta del archivo no existe 1 exitoso.
*/
int lectura_archivo(){
  FILE * fich;
  fich = fopen(nombre_archivo, "r");
  datos_archivo.operaciones=(operaciones_t*) malloc(100*sizeof(operaciones_t));
  int contador=0;
  if (fich==NULL)
  {
    printf("El archivo ingresado no es valido.\n");
    return 0;
  }
  
  //Lee línea a línea y escribe en pantalla hasta el fin de fichero
  while (fgets(cadena, 1024, (FILE * ) fich)) {
    if(strstr(cadena, "=") != NULL){
      operaciones_t operacion;
      if(strstr(cadena, "INSERT=") != NULL){
        int cadena_len=strlen(cadena);
        char valor[VALORPALABRA]="";
        int valor_counter=0;
        double resultado_mem=0;
        for (size_t i = 0; i < cadena_len; i++)
        {
          if(isdigit(cadena[i])!=0){
            valor[valor_counter]=cadena[i];
            valor_counter++;
          }
        }
        operacion.tipo_operacion=1;
        operacion.valor=atoi(valor);
        datos_archivo.agregadas++;
      }else if(strstr(cadena, "UPDATE=") != NULL){
        int cadena_len=strlen(cadena);
        char valor[VALORPALABRA]="";
        char posicion[VALORPALABRA]="";
        int valor_counter=0;
        int posicion_counter=0;
        double resultado_mem=0;
        int bandera=1;
        for (int i = 0; i < cadena_len; i++)
        {
          if(isdigit(cadena[i])!=0){
            if(bandera==1){
              valor[valor_counter]=cadena[i];
              valor_counter++;
            }else{
              posicion[posicion_counter]=cadena[i];
              posicion_counter++;
            }
          }
          if(cadena[i]==','){
            bandera=0;
          }
        }
        operacion.tipo_operacion=2;
        operacion.valor=atoi(valor);
        operacion.posicion=atoi(posicion);
        datos_archivo.actualizaciones++;
      }else if(strstr(cadena, "DELETE=") != NULL){
        int cadena_len=strlen(cadena);
        char valor[VALORPALABRA]="";
        int valor_counter=0;
        double resultado_mem=0;
        for (size_t i = 0; i < cadena_len; i++)
        {
          if(isdigit(cadena[i])!=0){
            valor[valor_counter]=cadena[i];
            valor_counter++;
          }
        }
        operacion.tipo_operacion=3;
        operacion.posicion=atoi(valor);
        datos_archivo.eliminadas++;
      }else if(strstr(cadena, "SEARCH=") != NULL){
        int cadena_len=strlen(cadena);
        char valor[VALORPALABRA]="";
        int valor_counter=0;
        double resultado_mem=0;
        for (size_t i = 0; i < cadena_len; i++)
        {
          if(isdigit(cadena[i])!=0){
            valor[valor_counter]=cadena[i];
            valor_counter++;
          }
        }
        operacion.tipo_operacion=4;
        operacion.valor=atoi(valor);
        datos_archivo.busquedas++;
      }
      datos_archivo.operaciones[contador]=operacion;
      contador++;
    }
  }
  datos_archivo.numero_operaciones=contador;
  return 1;
}

/**
  @brief Funcion para la presentacion de resultados como la lista final, numero de operaciones realizadas por insertar,
  en los casos de busquedas, eliminadas y actualizacion se muestran las exitosas y fallidas.

  @param No recibe parametros

  @returns No retorna nada.
*/
void presentacion_resultados(){
  printf("Lista resultante: ");
  imprimir_lista(referencia_inicio);
  printf("*************Operaciones realizadas*************\n");
  printf("Inserciones: %d\n",datos_archivo.agregadas);
  printf("Actualizaciones: %d--Exitosas: %d --Fallidas: %d\n",datos_archivo.actualizaciones,datos_archivo.actualizaciones_exitosas,datos_archivo.actualizaciones_fallidas);
  printf("Busquedas: %d--Exitosas: %d --Fallidas: %d\n",datos_archivo.busquedas,datos_archivo.busquedas_exitosas,datos_archivo.busquedas_fallidas);
  printf("Eliminadas: %d--Exitosas: %d --Fallidas: %d\n",datos_archivo.eliminadas,datos_archivo.eliminadas_exitosas,datos_archivo.eliminadas_fallidas);
}

int main(int argc, char **argv) 
{ 
  if(argc!=2) {
    printf("Parametros: ./main direccion_achivo.txt\n");
    exit(1);
  }
  nombre_archivo=argv[1];
  if(lectura_archivo()==0){
    return -1;
  }
  //Referencia al puntero inicial
  referencia_inicio=NULL;
  //Inicializacion de mutex
  pthread_mutex_init( & mutex_insert, NULL);
  pthread_mutex_init( & mutex_eliminar, NULL);
  pthread_mutex_init( & mutex_actualizar, NULL);
  //Inicializacion de lista de hilos por cada operacion leida
  pthread_t * lista = malloc(datos_archivo.numero_operaciones * sizeof(pthread_t * ));
  for (int i = 0; i < datos_archivo.numero_operaciones; i++)
  {
    //Generacion de numero random para las operaciones
    int numero = rand() % 3;
    //Temporal con la estructura que se le dara a cada hilo y asignacion de valores
    operaciones_t * lol = malloc(sizeof(operaciones_t));
    ( * lol).posicion =datos_archivo.operaciones[i].posicion;
    ( * lol).tipo_operacion =datos_archivo.operaciones[i].tipo_operacion;
    ( * lol).valor =datos_archivo.operaciones[i].valor;
    ( * lol).tiempo_espera =numero;
    //Ejecucion de hilos dependiendo del tipo de operacion
    if(datos_archivo.operaciones[i].tipo_operacion==1){
      pthread_create( & lista[i], NULL, (void * ) & agregar_elemento, (void * ) lol);
    }else if(datos_archivo.operaciones[i].tipo_operacion==2){
      pthread_create( & lista[i], NULL, (void * ) & actualizar_elemento, (void * ) lol);
    }else if(datos_archivo.operaciones[i].tipo_operacion==3){
      pthread_create( & lista[i], NULL, (void * ) & eliminar_elemento, (void * ) lol);
    }else{
      pthread_create( & lista[i], NULL, (void * ) & buscar_elemento, (void * ) lol);
    }
    
  }
  //Espera por finalizacion del hilo
  for (size_t i = 0; i < datos_archivo.numero_operaciones; i++) {
    pthread_join(lista[i], NULL);
  }
  //Presentacion de resultados
  presentacion_resultados();
  return 0; 
} 
